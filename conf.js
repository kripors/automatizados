let SpecReporter = require("jasmine-spec-reporter").SpecReporter;

exports.config = {
  framework: "jasmine2",
  directConnect: true,
  //seleniumAddress: "http://localhost:4444/wd/hub",
  capabilities: {
    browserName: "chrome",
    chromeOptions: {
      w3c: false,
      args: [
        "--headless",
        "--no-sandbox",
        "--disable-dev-shm-usage",
        "--window-size=1280x1024",
        "--disable-extensions",
        "--disable-gpu",
        "--start-maximized"
      ]
    },
    /*shardTestFiles: true,
        maxInstances: 4,*/
    specs: ["spec/*.spec.js"]
  },

  onPrepare: function() {
    browser.waitForAngularEnabled(false);
    var AllureReporter = require("jasmine-allure-reporter");
    jasmine.getEnv().addReporter(new AllureReporter());
    jasmine.getEnv().afterEach(function(done) {
      browser.takeScreenshot().then(function(png) {
        allure.createAttachment(
          "Screenshot",
          function() {
            return new Buffer(png, "base64");
          },
          "image/png"
        )();
        done();
      });
    });
    jasmine.getEnv().addReporter(
      new SpecReporter({
        displayFailuresSummary: true,
        displayFailuredSpec: true,
        displaySuiteNumber: true,
        displaySpecDuration: true
      })
    );
  }
};
