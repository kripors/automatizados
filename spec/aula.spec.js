/**
 * @file aula.spec.js
 */

browser.ignoreSynchronization = true;
var PaginaHome = require("../po/home.po.js");
var PaginaDeLogin = require("../po/login.po");
var PaginaDeCadastro = require("../po/cadastro.po");
const protractorHelper = require("protractor-helper");

describe("Fazer Busca e Ver Aula", function() {
  var paginaHome = new PaginaHome();
  var paginaDeLogin = new PaginaDeLogin();
  var paginaDeCadastro = new PaginaDeCadastro();

  it("Fazer Busca", function() {
    paginaDeLogin.visit();
    protractorHelper.fillFieldWithText(emailField, "kripors@gmail.com", 3000);
    protractorHelper.fillFieldWithText(passField, "Quanticcode001", 3000);
    protractorHelper.click(botaoLogin, 3000);
    protractorHelper.fillFieldWithTextAndPressEnter(
      campoDeBusca,
      "Bitcoin",
      50000
    );
    expect(protractorHelper.waitForUrlToBeEqualToExpectedUrl(Busca, 5000));
  });
  it("Ver Aula", function() {
    paginaDeLogin.visit();
    protractorHelper.fillFieldWithText(emailField, "kripors@gmail.com", 3000);
    protractorHelper.fillFieldWithText(passField, "Quanticcode001", 3000);
    protractorHelper.click(botaoLogin, 3000);
    protractorHelper.fillFieldWithTextAndPressEnter(
      campoDeBusca,
      "Bitcoin",
      100000
    );
    protractorHelper.click(botaoCursoBtn, 5000);
    protractorHelper.hoverAndClick(botaoCapitulo, 5000);
    expect(protractorHelper.waitForElementVisibility(janelaVideo, 5000));
  });
});
