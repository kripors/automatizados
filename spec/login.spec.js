/**
 * @file login.spec.js
 */
browser.ignoreSynchronization = true;
var PaginaDeLogin = require("../po/login.po");
const protractorHelper = require("protractor-helper");

describe("Login ", function() {
  var paginaDeLogin = new PaginaDeLogin();
  var EC = protractor.ExpectedConditions;

  afterEach(function() {
    var button = element(by.css("div > #navbarDropdownMenuLink"));
    protractorHelper.hoverAndClick(button, 10000);
    protractorHelper.click(botaoSair, 10000);
  });

  it("Login", function() {
    paginaDeLogin.visit();
    protractorHelper.fillFieldWithText(emailField, "kripors@gmail.com", 3000);
    protractorHelper.fillFieldWithText(passField, "Quanticcode001", 3000);
    protractorHelper.click(botaoLogin, 3000);
    expect(protractorHelper.waitForUrlToBeEqualToExpectedUrl(home, 10000));
  });

  it("Login BV", function() {
    paginaDeLogin.visit();
    protractorHelper.fillFieldWithText(
      emailField,
      "_saintpaul@bv.com.br",
      3000
    );
    protractorHelper.click(botaoLogin, 3000);
    protractorHelper.click(botaoLogBv, 30000);
    protractorHelper.fillFieldWithText(emailBv, "_saintpaul@bv.com.br", 3000);
    protractorHelper.fillFieldWithText(passBV, "mestEwr8", 3000);
    protractorHelper.click(loginBvBtn, 3000);
    expect(protractorHelper.waitForUrlNotToBeEqualToExpectedUrl(home, 10000));
  });

  xit("Login Facebook", function() {
    paginaDeLogin.visit();
    protractorHelper.click(botaoFacebook, 3000);
    var winHandles = browser.getAllWindowHandles();

    winHandles.then(function(handles) {
      var parenwindows = handles[0];
      var facebookwin = handles[1];

      browser.switchTo().window(facebookwin);
      protractorHelper.fillFieldWithText(faceMail, "usuário");
      protractorHelper.fillFieldWithText(facePsswd, "senha");
      protractorHelper.click(faceBtn, 3000);
      browser.switchTo().window(parenwindows);
      expect(protractorHelper.waitForUrlToBeEqualToExpectedUrl(home, 10000));
    });
  });

  xit("Login Google", function() {
    paginaDeLogin.visit();
    protractorHelper.click(botaoGoogle, 3000);
    var winHandles = browser.getAllWindowHandles();

    winHandles.then(function(handles) {
      var parenWindows = handles[0];
      var googleWin = handles[1];

      browser.switchTo().window(googleWin);
      protractorHelper.fillFieldWithText(googleMail, "usuário", 3000);
      protractorHelper.click(googlebtnid, 3000);
      protractorHelper.fillFieldWithText(googlepsswd, "senha", 3000);
      protractorHelper.click(googlebtnps, 3000);
      browser.switchTo().window(parenWindows);
      expect(protractorHelper.waitForUrlToBeEqualToExpectedUrl(home, 10000));
    });
  });
  xit("Login Linkedin", function() {
    paginaDeLogin.visit();
    protractorHelper.click(botaoLinkedin, 3000);
    var winHandles = browser.getAllWindowHandles();

    winHandles.then(function(handles) {
      var parentWindow = handles[0];
      var linkedWin = handles[1];

      browser.switchTo().window(linkedWin);
      protractorHelper.fillFieldWithText(linkedinMail, "usuário", 3000);
      protractorHelper.fillFieldWithText(linkedinPass, "senha", 3000);
      protractorHelper.click(linkedinBtnLog, 3000);
      browser.switchTo().window(parentWindow);
      expect(protractorHelper.waitForUrlToBeEqualToExpectedUrl(home, 10000));
    });
  });
});
