//import { protractor } from "protractor/built/ptor";

/**
 * @file login.spec.js
 */

browser.ignoreSynchronization = true;
var PaginaDeCadastro = require("../po/cadastro.po");
var PaginaDeLogin = require("../po/login.po.js");
const protractorHelper = require("protractor-helper");

describe("Cadastro", () => {
  var paginaDeLogin = new PaginaDeLogin();
  var paginaDeCadastro = new PaginaDeCadastro();

  it("Plano com Voucher", () => {
    browser.driver
      .manage()
      .window()
      .maximize();
    paginaDeLogin.visit();
    protractorHelper.click(botao, 3000);
    protractorHelper.fillFieldWithText(nomeCadastro, "Teste", 3000);
    protractorHelper.fillFieldWithText(sobreNomeCadastro, "Teste", 3000);
    protractorHelper.fillFieldWithText(celCadastro, "51997661515", 3000);
    protractorHelper.fillFieldWithText(
      emailCadastro,
      "teste001@lit.com.br",
      3000
    );
    protractorHelper.fillFieldWithText(passCadastro, "Lit@S@intP4ul2#1*", 3000);
    protractorHelper.fillFieldWithText(
      confirPassCadastro,
      "Lit@S@intP4ul2#1*",
      3000
    );
    protractorHelper.hoverAndClick(aceito, 3000);
    protractorHelper.click(botaoCadastrar, 3000);
    protractorHelper.click(botaoVoucher, 10000);
    protractorHelper.fillFieldWithText(
      inputVoucher,
      "TESTVOUCHER-100-0-bEUy68G9",
      5000
    );
    protractorHelper.hoverAndClick(applyVoucher, 3000);
    browser.driver.sleep(2000);
    protractorHelper.hoverAndClick(applyVoucher, 3000);
    protractorHelper.click(botaoComecar, 5000);
    protractorHelper.hoverAndClick(botaoMercadoF, 3000);
    protractorHelper.hoverAndClick(botaoNegocioD, 3000);
    protractorHelper.hoverAndClick(botaoProxPasso, 3000);
    protractorHelper.hoverAndClick(botaoSim, 3000);
    protractorHelper.fillFieldWithText(botaoAnoCo, "2006", 3000);
    protractorHelper.fillFieldWithTextAndPressEnter(
      botaoCurso,
      "direito",
      5000
    );
    protractorHelper.hoverAndClick(botaoNext, 3000);
    protractorHelper.fillFieldWithTextAndPressEnter(
      botaoCargo,
      "analista",
      5000
    );
    protractorHelper.fillFieldWithTextAndPressEnter(
      botaoEmpresa,
      "Quantic Code",
      3000
    );
    protractorHelper.fillFieldWithTextAndPressEnter(
      botaoDataIn,
      "02122019",
      3000
    );
    protractorHelper.click(botaoJobAtual, 3000);
    protractorHelper.fillFieldWithText(
      botaoSobreExp,
      "Analista na quantic code",
      3000
    );
    protractorHelper.click(botaoPp, 3000);
    protractorHelper.fillFieldWithTextAndPressEnter(botaoCep, "91880800", 5000);
    protractorHelper.hoverAndClick(botaoPesquisar, 3000);
    browser.driver.sleep(1000);
    protractorHelper.fillFieldWithText(botaoNumero, "135", 3000);
    protractorHelper.fillFieldWithText(botaoTelefone, "51997661515", 3000);
    protractorHelper.fillFieldWithText(botaoNacionalidade, "Brasileiro", 3000);
    protractorHelper.click(botaoGenero, 3000);
    protractorHelper.click(botaoSeguir);
    protractorHelper.click(botaoEscolaridade);
    protractorHelper.click(botaoComo);
    protractorHelper.fillFieldWithTextAndPressEnter(
      botaoDataN,
      "15111979",
      3000
    );
    protractorHelper.click(botaoContinue);
    browser.driver.sleep(3000);
    expect(browser.getCurrentUrl()).toMatch(home);
  });
});
