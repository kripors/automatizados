/**
 * @file home.po.js
 */

const protractorHelper = require("protractor-helper");

var PaginaHome = function() {
  campoDeBusca = element(by.css(".navbar-lit-search"));
  botaoLit = element(by.css(".navbar-brand > img"));
  botaoCursoBtn = element(by.css(".card-text:nth-child(2)"));
  botaoCapitulo = element(by.id("1_el6318za"));
  janelaVideo = element(by.css("video"));
};

module.exports = PaginaHome;
