/**
 * @file cadastro.po.js
 */

const protractorHelper = require("protractor-helper");

var PaginaDeLogin = function() {
  botao = element(by.linkText("LIT"));
  nomeCadastro = element(by.name("name"));
  sobreNomeCadastro = element(by.id("inputLastName"));
  emailCadastro = element(by.id("inputEmail"));
  passCadastro = element(by.id("inputPassword"));
  confirPassCadastro = element(by.id("inputPasswordConfirm"));
  celCadastro = element(by.id("phoneNumber"));
  aceito = element(by.css(".cr-icon"));
  botaoCadastrar = element(by.css(".sc-chPdSV"));
  plano = element(by.css("#monthly5dfb8b6a6271cb612a771719 > .sc-jhAzac"));
  botaoVoucher = element(by.id("btn-voucher"));
  inputVoucher = element(by.id("input-apply-voucher"));
  applyVoucher = element(by.id("btn-select-voucher"));
  botaoAvancar = element(by.id("next-button"));
  numeroCc = element(by.name("CCnumber"));
  nomeCc = element(by.name("CCname"));
  cpfCc = element(by.name("cpf"));
  nascimentoCc = element(by.name("nascimento"));
  mesVencCc = element(by.cssContainingText("option", "Março"));
  anoVenCC = element(by.cssContainingText("option", "2030"));
  cvvCc = element(by.name("cvc"));
  botaoConfirmar = element(by.css(".btn-blue"));
  botaoComecar = element(by.css("#btn-start-customize"));
  botaoMercadoF = element(by.css("#btn-0"));
  botaoNegocioD = element(by.css("#btn-3"));
  botaoProxPasso = element(by.css("#btn-next-step"));
  botaoSim = element(by.id("btn-yes"));
  botaoCurso = element(by.id("react-select-2-input"));
  botaoAnoCo = element(by.id("input-conclusion-year"));
  botaoNext = element(by.id("next-step"));
  botaoCargo = element(by.id("react-select-3-input"));
  botaoEmpresa = element(by.id("company-name"));
  botaoDataIn = element(by.id("start-date"));
  botaoJobAtual = element(by.css(".css-1hwfws3"));
  botaoSobreExp = element(by.id("about-profession"));
  botaoPp = element(by.css(".btn"));
  botaoCep = element(by.id("input-cep"));
  botaoPesquisar = element(by.id("btn-searchCEP"));
  botaoNumero = element(by.id("input-number"));
  botaoTelefone = element(by.id("input-phone"));
  botaoNacionalidade = element(by.id("input-nacionality"));
  botaoGenero = element(by.cssContainingText("option", "Masculino"));
  botaoSeguir = element(by.id("btn-next-step"));
  botaoEscolaridade = element(
    by.cssContainingText("option", "Ensino superior")
  );
  botaoComo = element(by.cssContainingText("option", "Bradesco"));
  botaoDataN = element(by.id("input-birthday"));
  botaoContinue = element(by.id("btn-continue"));
  botaoAceitarCookie = element(by.id("hs-eu-confirmation-button"));
};

module.exports = PaginaDeLogin;
