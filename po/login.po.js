//PaginaDeLogin.po.js
var portal = "https://app.lit.com.br";

var PaginaDeLogin = function() {
  home = portal + "/home";
  Busca = portal + "/busca";
  emailField = element(by.id("inputEmail"));
  passField = element(by.id("inputPassword"));
  botaoLogin = element(by.css(".sc-chPdSV"));
  botaoLogout = element(by.css(".show > .dropdown-item:nth-child(3)"));
  botaoLogBv = element(by.css(".idp:nth-child(3) .largeTextNoWrap"));
  botaoSair = element(by.css(".show > .dropdown-item:nth-child(3)"));
  emailBv = element(by.id("userNameInput"));
  passBV = element(by.id("passwordInput"));
  loginBvBtn = element(by.id("submitButton"));
  botaoFacebook = element(by.css("div:nth-child(1) > .btn"));
  faceMail = element(by.css("#email"));
  facePsswd = element(by.id("pass"));
  faceBtn = element(by.name("login"));
  botaoGoogle = element(by.css("div:nth-child(2) > .btn > img"));
  googleMail = element(by.id("identifierId"));
  googlepsswd = element(by.name("password"));
  googlebtnid = element(by.css("#identifierNext .RveJvd"));
  googlebtnps = element(by.css("#passwordNext .RveJvd"));
  botaoLinkedin = element(by.css(".false > img"));
  linkedinMail = element(by.id("username"));
  linkedinPass = element(by.id("password"));
  linkedinBtnLog = element(by.css(".btn__primary--large"));
};

PaginaDeLogin.prototype.visit = function() {
  browser.get(portal);
};

module.exports = PaginaDeLogin;
